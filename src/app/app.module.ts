import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {InviteComponent} from './personne/invite/invite/invite.component';
import {EnseignantModule} from './personne/enseignant/enseignant.module';
import {InviteService} from './shared/service/personne/invite/invite.service';
import {MessageService} from './shared/service/message.service';
import {EnseignantService} from './shared/service/personne/enseignant/enseignant.service';
import {MatiereService} from './shared/service/matiere/matiere.service';
import {AuthGuard} from './shared/garde-acces/matiere/auth.guard';
import {CanDeactivateGuard} from './shared/garde-acces/matiere/can-desactivate.guard';
import {HttpCaheService} from './shared/intercepteur/cache/http-cahe.service';
import {MatiereModule} from './matiere/matiere.module';
import {MatiereRoutingModule} from './matiere/matiere-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from './material.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TimingIntercepteur} from './shared/intercepteur/timing-intercepteur';
import {CachingIntercepteur} from './shared/intercepteur/caching-intercepteur';
import {AcceuilComponent} from './shared/cadre/acceuil/acceuil.component';

@NgModule({
  declarations: [
    AppComponent,
    InviteComponent,
    AcceuilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    EnseignantModule,
    MatiereModule,
    MatiereRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [InviteService,
    MessageService,
    EnseignantService,
    MatiereService,
    AuthGuard,
    CanDeactivateGuard,
    HttpCaheService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: TimingIntercepteur,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CachingIntercepteur,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
