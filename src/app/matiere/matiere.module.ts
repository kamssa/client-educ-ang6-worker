import {NgModule} from '@angular/core';
import {CreerMatiereComponent} from './creer-matiere/creer-matiere.component';
import {EditerEnseignantComponent} from '../personne/enseignant/editer-enseignant/editer-enseignant.component';
import {DetailEnseignantComponent} from '../personne/enseignant/detail-enseignant/detail-enseignant.component';
import {ManageEnseignantComponent} from '../personne/enseignant/manage-enseignant/manage-enseignant.component';
import {ListeEnseignantComponent} from '../personne/enseignant/liste-enseignant/liste-enseignant.component';
import {DetailMatiereComponent} from './detail-matiere/detail-matiere.component';
import {EditerMatiereComponent} from './editer-matiere/editer-matiere.component';
import {ListeMatiereComponent} from './liste-matiere/liste-matiere.component';
import {ManageMatiereComponent} from './manage-matiere/manage-matiere.component';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {MatiereRoutingModule} from './matiere-routing.module';


@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    MatiereRoutingModule
  ],
  declarations: [
    CreerMatiereComponent,
    DetailMatiereComponent,
    EditerMatiereComponent,
    ListeMatiereComponent,
    ManageMatiereComponent
  ]
})
export class MatiereModule {
}
