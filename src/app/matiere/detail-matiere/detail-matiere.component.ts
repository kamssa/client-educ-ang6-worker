import { Component, OnInit } from '@angular/core';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Matiere} from '../../shared/modele/matiere/matiere';
import {switchMap} from 'rxjs/internal/operators';

@Component({
  selector: 'app-detail-matiere',
  templateUrl: './detail-matiere.component.html',
  styleUrls: ['./detail-matiere.component.scss']
})
export class DetailMatiereComponent implements OnInit {
  matiere: Matiere;
  title = 'Detail des matieres';

  constructor(private matiereServce: MatiereService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.paramMap.pipe(switchMap((params: ParamMap) =>
      this.matiereServce.getMatiereById(+params.get('id'))))
      .subscribe(res => {
        this.matiere = res.body;

      });

  }

  editerMatiere() {
    this.router.navigate(['/matiere/liste', this.matiere.id, 'edite']);
  }

  supprimerMatiere() {
    console.log('supprimer matiere')
    this.matiereServce.supprimerUneMatiere(this.matiere.id)
      .subscribe(res => {
        console.log(res.messages);
      });
    // this.matiereServce.matiereSupprimer(this.matiere);
    this.router.navigate(['/matiere/liste']);

  }

  canDeactivate() {
    return window.confirm('avez vous le droit?');
  }
}
