import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CreerMatiereComponent} from './creer-matiere/creer-matiere.component';
import {ListeMatiereComponent} from './liste-matiere/liste-matiere.component';
import {EditerMatiereComponent} from './editer-matiere/editer-matiere.component';
import {ManageMatiereComponent} from './manage-matiere/manage-matiere.component';
import {AuthGuard} from '../shared/garde-acces/matiere/auth.guard';
import {CanDeactivateGuard} from '../shared/garde-acces/matiere/can-desactivate.guard';
import {DetailMatiereComponent} from './detail-matiere/detail-matiere.component';

const matiereRoute: Routes = [
  {
    path: 'matiere',
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ManageMatiereComponent,

      },
      {
        path: 'liste',
        canActivateChild: [AuthGuard],
        component: ListeMatiereComponent,
        children: [
          {
            path: ':id/detail',
            component: DetailMatiereComponent,
            canDeactivate: [CanDeactivateGuard]
          },
          {
            path: ':id/edite',
            component: EditerMatiereComponent,
          },
          {
            path: 'creer', component: CreerMatiereComponent,
          }

        ]

      }

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(matiereRoute)
  ],
  exports: [RouterModule],
  declarations: []
})
export class MatiereRoutingModule {
}
