import { Component, OnInit } from '@angular/core';
import {Matiere} from '../../shared/modele/matiere/matiere';
import {MatiereService} from '../../shared/service/matiere/matiere.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {switchMap} from 'rxjs/internal/operators';


@Component({
  selector: 'app-editer-matiere',
  templateUrl: './editer-matiere.component.html',
  styleUrls: ['./editer-matiere.component.scss']
})
export class EditerMatiereComponent implements OnInit {
  matiere: Matiere;
  titre = 'Modification du detail'


  constructor(private matiereService: MatiereService, private  route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.route.paramMap.pipe(switchMap((params: ParamMap) =>
      this.matiereService.getMatiereById(+params.get('id'))))
      .subscribe(res => {
        this.matiere = res.body;
      });
  }

  onSubmit() {
    console.log('Modification');
    this.matiereService.modifierUneMatiere(this.matiere)
      .subscribe(res =>
        console.log('la matiere modifiee: '));
    this.router.navigate(['/matiere/liste']);
  }

  annuler() {
    this.router.navigate(['/matiere/liste']);
  }
}
