import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditerMatiereComponent } from './editer-matiere.component';

describe('EditerMatiereComponent', () => {
  let component: EditerMatiereComponent;
  let fixture: ComponentFixture<EditerMatiereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditerMatiereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditerMatiereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
