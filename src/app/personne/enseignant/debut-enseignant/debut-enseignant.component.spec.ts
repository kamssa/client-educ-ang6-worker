import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebutEnseignantComponent } from './debut-enseignant.component';

describe('DebutEnseignantComponent', () => {
  let component: DebutEnseignantComponent;
  let fixture: ComponentFixture<DebutEnseignantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebutEnseignantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebutEnseignantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
