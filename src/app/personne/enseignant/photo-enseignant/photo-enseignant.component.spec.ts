import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoEnseignantComponent } from './photo-enseignant.component';

describe('PhotoEnseignantComponent', () => {
  let component: PhotoEnseignantComponent;
  let fixture: ComponentFixture<PhotoEnseignantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoEnseignantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoEnseignantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
