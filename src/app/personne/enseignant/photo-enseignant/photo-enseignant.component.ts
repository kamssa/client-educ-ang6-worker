import {Component, OnInit, ViewChild} from '@angular/core';
import {Enseignant} from '../../../shared/modele/personne/enseignant';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EnseignantService} from '../../../shared/service/personne/enseignant/enseignant.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import {switchMap} from 'rxjs/internal/operators';

@Component({
  selector: 'app-photo-enseignant',
  templateUrl: './photo-enseignant.component.html',
  styleUrls: ['./photo-enseignant.component.scss']
})
export class PhotoEnseignantComponent implements OnInit {
  enseignant: Enseignant;
  ensPhotoForm: FormGroup;
  enseignantImageFile: File;

  @ViewChild('enseignantImage') enseignant_image;


  constructor(private enseService: EnseignantService, private fb: FormBuilder, private route: ActivatedRoute, private  router: Router) {

  }

  ngOnInit() {
    this.route.paramMap.pipe(switchMap((params: ParamMap) =>
      this.enseService.getEnseignantById(+params.get('id'))))
      .subscribe(res => {
        this.enseignant = res.body;

      });
    this.initForm();
  }

  onSubmit() {
    const image = this.enseignant_image.nativeElement;
    if (image.files && image.files[0]) {
      this.enseignantImageFile = image.files[0];
    }
    const imageFile: File = this.enseignantImageFile;
    this.enseService.enregistrerPhoto(imageFile, this.enseignant.cni)
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          const percentDone = Math.round(100 * event.loaded / event.total);
          console.log(`Le fichier ${percentDone}% charger.`);
        } else if (event instanceof HttpResponse) {
          console.log('Le fichier est completement charger!');
        }
      });

    this.router.navigate(['enseignant/liste']);
  }

  initForm() {
    this.ensPhotoForm = this.fb.group({
      ensImg: ['']
    });
  }
  annuler() {
    this.router.navigate(['enseignant/liste']);
  }

}
