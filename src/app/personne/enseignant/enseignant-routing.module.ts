import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ManageEnseignantComponent} from './manage-enseignant/manage-enseignant.component';
import {ListeEnseignantComponent} from './liste-enseignant/liste-enseignant.component';
import {DebutEnseignantComponent} from './debut-enseignant/debut-enseignant.component';
import {EditerEnseignantComponent} from './editer-enseignant/editer-enseignant.component';
import {PhotoEnseignantComponent} from './photo-enseignant/photo-enseignant.component';
import {DetailEnseignantComponent} from './detail-enseignant/detail-enseignant.component';

const enseignantRoutes: Routes = [
  {
    path: 'enseignant',
    children: [

      {
        path: '', component: ManageEnseignantComponent
      },
      {
        path: 'liste',
        component: ListeEnseignantComponent,
        children: [
          {path: '', component: DebutEnseignantComponent},
          {
            path: 'creer', component: EditerEnseignantComponent
          },
          {path: 'creerPhoto', component: PhotoEnseignantComponent},

          {
            path: ':id', component: DetailEnseignantComponent
          },
          {
            path: ':id/editer', component: EditerEnseignantComponent
          },
          {path: ':id/photo', component: PhotoEnseignantComponent}

        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(enseignantRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class EnseignantRoutingModule {
}
