import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {InviteService} from '../../../shared/service/personne/invite/invite.service';
import {Iinvite} from '../../../shared/modele/personne/interface/personne/iinvite';
import {SelectionModel} from '@angular/cdk/collections';


@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {
  displayedColumns = ['ID', 'CNI', 'TITRE', 'NOM', 'PRENOM', 'RAISON', 'PROFIL', 'SELECT'];
  invites: Iinvite[] = [];
  dataSource = new MatTableDataSource<Iinvite>();
  selection = new SelectionModel<Iinvite>();

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private  inviteService: InviteService) {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.tousInvites();
  }

  tousInvites() {
    this.inviteService.getAllInvite()
      .subscribe(data => {
        this.invites = data.body;
        this.dataSource.data = data.body;
        console.log(data.body);
      });
  }
}

