import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss']
})
export class AcceuilComponent implements OnInit {
  title = 'GESTION ECOLE POUR UNE FORMATION DE QUALITE ET D EXCELLENCE';
  constructor(private router: Router) { }

  ngOnInit() {
  }
  onInvite() {
    this.router.navigate(['/invite']);
  }

  onEtudiant() {
    this.router.navigate(['/etudiant']);
  }

  onEnseignant() {
    this.router.navigate(['/enseignant']);
  }

  onAdministrateur() {
    this.router.navigate(['/administration']);
  }

  onMatiere() {
    this.router.navigate(['/matiere']);
  }
}
