export interface Iadresse {
  codePostal?: string;
  quartier?: string;
  ville?: string;
  email?: string;
  mobile?: string;
  bureau?: string;
  fixe?: string;
}
