import {Personne} from './personne';
import {Adresse} from '../adresse/adresse';
import {Telephone} from './telephone';

export class Enseignant extends Personne {


  constructor(public id?: number,
              public version?: number,
              public cni?: string,
              public titre?: string,
              public nom?: string,
              public prenom?: string,
              public login?: string,
              public password?: string,
              public actived?: boolean,
              public nomComplet?: string,
              public statut ?: string,
              public specialite?: string,
              public pathPhoto?: string,
              public adresse?: Adresse,
              public  telephones?: Telephone[],
              public type?: string) {
    super(id, version, titre, nom, prenom, cni, login, password, actived, nomComplet, pathPhoto, adresse, telephones, type);
  }
}
