import {Injectable} from '@angular/core';
import {Resultat} from '../../../modele/resultat';
import {Invite} from '../../../modele/personne/invite';


import {Iadresse} from '../../../modele/personne/interface/personne/iadresse';
import {Iinvite} from '../../../modele/personne/interface/personne/iinvite';
import {Observable, of } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';

import {MessageService} from '../../message.service';


@Injectable()
export class InviteService {
  private urlInvite = 'http://localhost:8080/typePersonnes/IN';
  private urlPersonne = 'http://localhost:8080/personnes';

  constructor(private http: HttpClient, private  messageService: MessageService) {

  }

  getAllInvite(): Observable<Resultat<Iinvite[]>> {
    return this.http.get<Resultat<Iinvite[]>>(this.urlInvite)
      .pipe(
        tap(() => this.log('methode  getAll du service invite')),
        catchError(this.handleError<Resultat<Iinvite[]>>('getAllInvite', new Resultat<Iinvite[]>(null,[],[])))
      );


  }

  ajoutInvite(invite: Invite): Observable<Resultat<Invite>> {
    const add: Iadresse = {
      codePostal: invite.adresse.codePostal,
      quartier: invite.adresse.quartier,
      ville: invite.adresse.ville,
      email: invite.adresse.email,
      mobile: invite.adresse.mobile,
      bureau: invite.adresse.bureau,
      fixe: invite.adresse.fixe
    };
    const inv: Iinvite = {
      titre: invite.titre,
      nom: invite.nom,
      prenom: invite.prenom,
      cni: invite.cni,
      login: invite.login,
      password: invite.password,
      actived: invite.actived,
      description: invite.description,
      adresse: add,
      type: 'IN',
      raison: invite.raison,
      profil: invite.profil,
      societe: invite.societe,
      statut: invite.statut
    };
    return this.http.post<Resultat<Invite>>('http://localhost:8080/personnes', inv)
      .pipe(
        tap(res => console.log(res, 'methode ajout du service invite')),
        catchError(this.handleError<Resultat<Invite>>('ajoutInvite'))
      );
  }

  modifierUnInvite(modifInvite: Invite): Observable<Resultat<Invite>> {
    let add: Iadresse;
    add = {
      codePostal: modifInvite.adresse.codePostal,
      quartier: modifInvite.adresse.quartier,
      ville: modifInvite.adresse.ville,
      email: modifInvite.adresse.email,
      mobile: modifInvite.adresse.mobile,
      bureau: modifInvite.adresse.bureau,
      fixe: modifInvite.adresse.fixe
    };
    let inviteModif: Iinvite
    inviteModif = {
      id: modifInvite.id,
      version: modifInvite.version,
      titre: modifInvite.titre,
      nom: modifInvite.nom,
      prenom: modifInvite.prenom,
      cni: modifInvite.cni,
      login: modifInvite.login,
      password: modifInvite.password,
      actived: modifInvite.actived,
      description: modifInvite.description,
      adresse: add,
      type: 'IN',
      raison: modifInvite.raison,
      profil: modifInvite.profil,
      societe: modifInvite.societe,
      statut: modifInvite.statut
    };
    return this.http.put<Resultat<Invite>>(this.urlPersonne, inviteModif)
      .pipe(
        tap(res => console.log(res.body, 'methode modif du service invite')),
        catchError(this.handleError<Resultat<Invite>>('modifierUnInvite')));
  }

  // supprimer un invite
  supprimerUnInvite(id: number): Observable<Resultat<boolean>> {
    return this.http.delete<Resultat<boolean>>(`${this.urlPersonne}/${id}`)
      .pipe(
        tap(res => console.log(res.body, 'methode supprime du service invite')),
        catchError(this.handleError<Resultat<boolean>>('supprimerUnInvite')));
  }

  private log(message: string) {
    this.messageService.add('inviteService: ' + message);
  }

  ///////////////////////////////////////////
  // recuper les erreurs
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {


      console.error(error);


     /* this.log(`${operation} failed: ${error.message}`);*/


      return of(result as T);
    };
  }
}
