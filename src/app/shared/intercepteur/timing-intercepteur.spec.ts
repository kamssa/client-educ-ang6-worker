import { TestBed, inject } from '@angular/core/testing';

import { TimingIntercepteur } from './timing-intercepteur';

describe('TimingIntercepteurService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimingIntercepteur]
    });
  });

  it('should be created', inject([TimingIntercepteur], (service: TimingIntercepteur) => {
    expect(service).toBeTruthy();
  }));
});
