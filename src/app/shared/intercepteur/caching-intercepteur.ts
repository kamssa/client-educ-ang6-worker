import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {HttpCaheService} from './cache/http-cahe.service';
import {concat, tap} from 'rxjs/internal/operators';


@Injectable()
export class CachingIntercepteur implements HttpInterceptor {

  constructor(private cache: HttpCaheService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (req.method !== 'GET') {
      return next.handle(req);
    }

    let maybeCachedResponse: Observable<HttpEvent<any>> = of();


    const cachedResponse = this.cache.get(req);

    if (cachedResponse) {
      maybeCachedResponse = of(cachedResponse);
      console.log('la reponse du cache', maybeCachedResponse);
    }

    const networkResponse = next.handle(req).pipe(
      tap(event => {

        if (event instanceof HttpResponse) {
          this.cache.put(req, event); // mise a jour du cache.
          console.log('mis a jour du cache', event);
        }
        console.log('reponse du backend', networkResponse);
      })
    );
    return networkResponse;

  }
}
