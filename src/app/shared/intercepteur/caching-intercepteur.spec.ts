import { TestBed, inject } from '@angular/core/testing';

import { CachingIntercepteur } from './caching-intercepteur';

describe('CachingIntercepteurService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CachingIntercepteur]
    });
  });

  it('should be created', inject([CachingIntercepteur], (service: CachingIntercepteur) => {
    expect(service).toBeTruthy();
  }));
});
